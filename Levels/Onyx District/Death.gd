extends Area

# Called when the node enters the scene tree for the first time.
func _ready():
# warning-ignore:return_value_discarded
	connect("body_entered", self, "_on_collided")

func _on_collided(body):
	if body.has_method("take_damage"):
		body.take_damage(200, null, body.player_id)
