extends Node

# Default game port
const DEFAULT_PORT = 10567

# Maximum number of players
const MAX_PEERS = 16

# Name for local player
var player_name = "Jack Burton"

# ID and Names of all players in id:name format
var players = {}
var players_ready = []

var game_mode

# Signals for menu GUI
signal player_list_updated()
signal connection_failed()
signal connection_succeeded()
signal loading_started()
signal game_started()
signal game_ended()
signal game_errored(what)
signal score_updated()

func _ready():
# warning-ignore:return_value_discarded
	get_tree().connect("network_peer_disconnected", self,"_on_player_disconnected")
# warning-ignore:return_value_discarded
	get_tree().connect("connected_to_server", self, "_on_connected_ok")
# warning-ignore:return_value_discarded
	get_tree().connect("connection_failed", self, "_on_connected_fail")
# warning-ignore:return_value_discarded
	get_tree().connect("server_disconnected", self, "_on_server_disconnected")

	reset_state()

# Callback from SceneTree
func _on_player_disconnected(id):
	players.erase(id)
	if has_node("/root/Level/Players/" + str(id)):
		get_tree().get_root().get_node("Level/Players/" + str(id)).queue_free()
	emit_signal("player_list_updated")

# Callback from SceneTree, only for clients (not server)
func _on_connected_ok():
	rpc_id(1, "register_player", get_tree().get_network_unique_id(), player_name)
	emit_signal("connection_succeeded")

# Callback from SceneTree, only for clients (not server)
func _on_connected_fail():
	get_tree().set_network_peer(null) # Remove peer
	emit_signal("connection_failed")

# Callback from SceneTree, only for clients (not server)
func _on_server_disconnected():
	get_tree().set_network_peer(null)
	reset_state()
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	emit_signal("game_errored", "Host disconnected")


# Lobby management

remote func register_player(id, new_player_name):
	assert(get_tree().is_network_server())

	if game_mode.in_progress:
		for p_id in players:
			game_mode.rpc_id(p_id, "add_player", id)
	
	players[id] = new_player_name

	rpc("sync_player_list", players)
	rpc_id(id, "sync_game_mode", game_mode.game_mode_name)
	game_mode.sync_all_id(id)
	if game_mode.in_progress:
		rpc_id(id, "prepare_game")

func host_game(new_player_name, new_game_mode, new_level_list:Array):
	player_name = new_player_name
	players[1] = player_name
	var host = NetworkedMultiplayerENet.new()
	host.create_server(DEFAULT_PORT, MAX_PEERS)
	get_tree().set_network_peer(host)
	rpc("sync_game_mode", new_game_mode)
	if new_level_list.size() > 0:
		game_mode.level_queue = new_level_list.duplicate()

func join_game(ip, new_player_name):
	player_name = new_player_name
	var host = NetworkedMultiplayerENet.new()
	host.create_client(ip, DEFAULT_PORT)
	get_tree().set_network_peer(host)

remote func prepare_game():
	emit_signal("loading_started")
	get_tree().set_pause(true)

	if get_tree().is_network_server():
		game_mode.load_next_level()
		rpc("prepare_game")
		player_ready(1)
	else:
		game_mode.load_next_level()
		if game_mode.in_progress:
			start_game()
		else:
			rpc_id(1, "player_ready", get_tree().get_network_unique_id())

remotesync func player_ready(player_id):
	assert(get_tree().is_network_server())
	assert(player_id in players)
	assert(not player_id in players_ready)

	players_ready.append(player_id)

	if game_mode.in_progress:
		rpc_id(player_id, "start_game")
	elif players_ready.size() == players.size():
		game_mode.in_progress = true
		for p_id in players:
			game_mode.exit_spawn_queue(p_id)
			if p_id != 1:
				game_mode.sync_all_id(p_id)
		rpc("start_game")

remotesync func start_game():
	emit_signal("game_started")
	get_tree().set_pause(false)
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)

remote func end_game():
	if get_tree().is_network_server():
		rpc("end_game")
	get_tree().get_root().get_node("Level").queue_free()
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	emit_signal("game_ended")

remotesync func sync_player_list(player_list):
	if not get_tree().is_network_server():
		players.clear()
		players = player_list.duplicate(true)

	emit_signal("player_list_updated")

remotesync func sync_game_mode(server_game_mode):
	if server_game_mode == "Deathmatch":
		game_mode = load("res://GameModes/Deathmatch.gd").new()
	add_child(game_mode)


# Misc Methods

func get_player_list():
	return players.values()

func reset_state():
	players = {}
	players_ready = []

	game_mode = null
