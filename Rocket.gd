extends Spatial

var SPEED = 70
var DAMAGE = 20

var LIFETIME = 4
var timer = 0

var has_collided = false

var player = 0

func _ready():
# warning-ignore:return_value_discarded
	get_node("Area").connect("area_entered", self, "on_collided")
# warning-ignore:return_value_discarded
	get_node("Area").connect("body_entered", self, "on_collided")
	
func _physics_process(delta):
	var forward_dir = global_transform.basis.z.normalized()
	global_translate(forward_dir * SPEED * delta)
	
	timer += delta
	if timer >= LIFETIME:
		queue_free()
		
func on_collided(body):
	if not has_collided:
		if body.has_method("take_damage") and body.player_id != player:
			body.rpc("take_damage", DAMAGE, global_transform, player)
		
	has_collided = true
	$Smoke.emitting = false
	$Fire.emitting = false
	$MeshInstance.hide()