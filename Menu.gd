extends Control

func _ready():
# warning-ignore:return_value_discarded
	GameState.connect("connection_succeeded", self, "_on_connection_succeeded")
# warning-ignore:return_value_discarded
	GameState.connect("connection_failed", self, "_on_connection_failed")
# warning-ignore:return_value_discarded
	GameState.connect("player_list_updated", self, "_on_player_list_updated")
# warning-ignore:return_value_discarded
	GameState.connect("loading_started", self, "_on_loading_started")
# warning-ignore:return_value_discarded
	GameState.connect("game_started", self, "_on_game_started")
# warning-ignore:return_value_discarded
	GameState.connect("game_ended", self, "_on_game_ended")
# warning-ignore:return_value_discarded
	GameState.connect("game_errored", self, "_on_game_errored")
# warning-ignore:return_value_discarded
	GameState.connect("score_updated", self, "_on_score_updated")

	var view_size = $Menu.get_viewport_rect().size.y
	$Menu/MarginContainer/Particles2D.position.y = view_size / 2 - 80
	var bg_material = $Menu/MarginContainer/Particles2D.process_material as ParticlesMaterial
	bg_material.emission_box_extents = Vector3(view_size / 2, 1, 1)

func _on_play_pressed():
	show_connect()

func _on_quit_pressed():
	get_tree().quit()

func _on_host_pressed():
	if (get_node("Menu/MarginContainer/MainMenu/Connect/MarginContainer/VBoxContainer/NameInput").text == ""):
		get_node("Menu/MarginContainer/MainMenu/Connect/MarginContainer/VBoxContainer/Error").show()
		get_node("Menu/MarginContainer/MainMenu/Connect/MarginContainer/VBoxContainer/Error").set_text("You need to have a player name!")
		return

	get_node("Menu/MarginContainer/MainMenu/Connect/MarginContainer/VBoxContainer/Error").set_text("")
	get_node("Menu/MarginContainer/MainMenu/Connect/MarginContainer/VBoxContainer/Error").hide()
	show_lobby()

	var player_name = get_node("Menu/MarginContainer/MainMenu/Connect/MarginContainer/VBoxContainer/NameInput").text
	GameState.host_game(player_name, "Deathmatch", ["Onyx District"])
	_on_player_list_updated()

func _on_join_pressed():
	if (get_node("Menu/MarginContainer/MainMenu/Connect/MarginContainer/VBoxContainer/NameInput").text == ""):
		get_node("Menu/MarginContainer/MainMenu/Connect/MarginContainer/VBoxContainer/Error").show()
		get_node("Menu/MarginContainer/MainMenu/Connect/MarginContainer/VBoxContainer/Error").set_text("You need to have a player name!")
		return

	if (get_node("Menu/MarginContainer/MainMenu/Connect/MarginContainer/VBoxContainer/HBoxContainer/IPInput").text == ""):
		get_node("Menu/MarginContainer/MainMenu/Connect/MarginContainer/VBoxContainer/Error").show()
		get_node("Menu/MarginContainer/MainMenu/Connect/MarginContainer/VBoxContainer/Error").set_text("You cannot connect to no IP!")
		return

	var ip = get_node("Menu/MarginContainer/MainMenu/Connect/MarginContainer/VBoxContainer/HBoxContainer/IPInput").text
	if (not ip.is_valid_ip_address()):
		get_node("Menu/MarginContainer/MainMenu/Connect/MarginContainer/VBoxContainer/Error").show()
		get_node("Menu/MarginContainer/MainMenu/Connect/MarginContainer/VBoxContainer/Error").set_text("Invalid IP Address!")
		return

	get_node("Menu/MarginContainer/MainMenu/Connect/MarginContainer/VBoxContainer/HBoxContainer/Join").disabled = true
	get_node("Menu/MarginContainer/MainMenu/Connect/MarginContainer/VBoxContainer/HBoxContainer/Host").disabled = true
	
	var player_name = get_node("Menu/MarginContainer/MainMenu/Connect/MarginContainer/VBoxContainer/NameInput").text
	GameState.join_game(ip, player_name)

func _on_startgame_pressed():
	GameState.prepare_game()

func _on_connection_succeeded():
	show_lobby()

func _on_connection_failed():
	get_node("Menu/MarginContainer/MainMenu/Connect/MarginContainer/VBoxContainer/HBoxContainer/Join").disabled = false
	get_node("Menu/MarginContainer/MainMenu/Connect/MarginContainer/VBoxContainer/HBoxContainer/Host").disabled = false
	get_node("Menu/MarginContainer/MainMenu/Connect/MarginContainer/VBoxContainer/Error").show()
	get_node("Menu/MarginContainer/MainMenu/Connect/MarginContainer/VBoxContainer/Error").set_text("Connection Failed")

func _on_loading_started():
	show_loading()

func _on_game_started():
	show_hud()

func _on_game_ended():
	show_lobby()

func _on_game_errored(error_message):
	show_main_menu()
	$Menu/MarginContainer/MainMenu/Connect/MarginContainer/VBoxContainer/Error.show()
	$Menu/MarginContainer/MainMenu/Connect/MarginContainer/VBoxContainer/Error.set_text(error_message)

func _on_score_updated():
	var id = get_tree().get_network_unique_id()
	$HUD/Kills/Counter.set_text(str(GameState.game_mode.kills[id]))

func _on_player_list_updated():
	var players = GameState.get_player_list()
	players.sort()
	get_node("Menu/MarginContainer/MainMenu/Lobby/MarginContainer/VBoxContainer/PlayerList").clear()
	for p in players:
		if p == GameState.player_name:
			get_node("Menu/MarginContainer/MainMenu/Lobby/MarginContainer/VBoxContainer/PlayerList").add_item(p + " (You)")
		else:
			get_node("Menu/MarginContainer/MainMenu/Lobby/MarginContainer/VBoxContainer/PlayerList").add_item(p)

	get_node("Menu/MarginContainer/MainMenu/Lobby/MarginContainer/VBoxContainer/StartGame").disabled=not get_tree().is_network_server()

func show_main_menu():
	$HUD.hide()
	$Menu/LoadingScreen.hide()
	$Menu/MarginContainer/MainMenu/Connect.hide()
	$Menu/MarginContainer/MainMenu/Lobby.hide()
	$Menu.show()
	$Menu/MarginContainer.show()
	$Menu/MarginContainer/MainMenu.show()
	$Menu/MarginContainer/MainMenu/MenuOptions.show()

func show_connect():
	$HUD.hide()
	$Menu/LoadingScreen.hide()
	$Menu/MarginContainer/MainMenu/MenuOptions.hide()
	$Menu/MarginContainer/MainMenu/Lobby.hide()
	$Menu.show()
	$Menu/MarginContainer.show()
	$Menu/MarginContainer/MainMenu.show()
	$Menu/MarginContainer/MainMenu/Connect.show()
	$Menu/MarginContainer/MainMenu/Connect/MarginContainer/VBoxContainer/NameInput.grab_focus()

func show_lobby():
	$HUD.hide()
	$Menu/LoadingScreen.hide()
	$Menu/MarginContainer/MainMenu/Connect.hide()
	$Menu/MarginContainer/MainMenu/MenuOptions.hide()
	$Menu.show()
	$Menu/MarginContainer.show()
	$Menu/MarginContainer/MainMenu.show()
	$Menu/MarginContainer/MainMenu/Lobby.show()

func show_loading():
	$HUD.hide()
	$Menu/MarginContainer.hide()
	$Menu/LoadingScreen.show()

func show_hud():
	$Menu.hide()
	$HUD.show()
	$HUD/CenterContainer/DeathMessage.hide()
	$HUD/CenterContainer/Crosshair.show()

func show_dead():
	$Menu.hide()
	$HUD.show()
	$HUD/CenterContainer/Crosshair.hide()
	$HUD/CenterContainer/DeathMessage.show()

func update_health(new_health):
	$HUD/Health/Counter.set_text(str(new_health))
