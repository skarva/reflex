extends KinematicBody

const GRAVITY = -33
const MAX_SPEED = 20
const ACCEL = 5
const DEACCEL = 15
const MAX_SLOPE_ANGLE = 45
var vel = Vector3()

var motion = Vector3()

var MOUSE_SENSITIVITY = 0.08
var INVERT_LOOK = 1

var camera
var torso

var JUMP_SPEED = 15
var is_jumping = false

var health = 100
var is_dead = false
const RESPAWN_TIME = 5
var respawn_timer = 0

var rocket

var grapple_ray
const MAX_GRAPPLE_DIST = 100
const MIN_GRAPPLE_DIST = 2
var is_grappling = false

const MAX_AIR_SPEED = 15

var player_id = 1
puppet var network_position = Vector3()
puppet var network_motion = Vector3()

func _ready():
	camera = get_node("Torso/Camera")
	torso = get_node("Torso")
	grapple_ray = get_node("Torso/RayCast")
	network_position = global_transform

func _input(event):
	if is_network_master() and event is InputEventMouseMotion and Input.get_mouse_mode() == Input.MOUSE_MODE_CAPTURED:
		torso.rotate_x(deg2rad(event.relative.y * MOUSE_SENSITIVITY * INVERT_LOOK))
		self.rotate_y(deg2rad(event.relative.x * MOUSE_SENSITIVITY * -1))

		var camera_rotation = torso.rotation_degrees
		camera_rotation.x = clamp(camera_rotation.x, -85, 85)
		torso.rotation_degrees = camera_rotation
	
	if is_network_master():
		if Input.is_action_just_pressed("ui_cancel"):
			if Input.get_mouse_mode() == Input.MOUSE_MODE_VISIBLE:
				Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
				get_tree().get_root().get_node("UI").show_hud()
			else:
				Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
				get_tree().get_root().get_node("UI").show_main_menu()

func _physics_process(delta):
	if not is_dead:
		processInput()
		processMovement(delta)
	else:
		processRespawn(delta)

func processInput():
	motion = Vector3()

	if is_network_master():
		var camera_transform = camera.get_global_transform()

		var move_vector = Vector2()

		if Input.is_action_pressed("move_forward"):
			move_vector.y += 1
		elif Input.is_action_pressed("move_backward"):
			move_vector.y -= 1
		if Input.is_action_pressed("move_left"):
			move_vector.x -= 1
		elif Input.is_action_pressed("move_right"):
			move_vector.x += 1

		move_vector = move_vector.normalized()

		motion += -camera_transform.basis.z.normalized() * move_vector.y
		motion += camera_transform.basis.x.normalized() * move_vector.x

		if Input.is_action_just_pressed("fire"):
			rpc("fire", player_id, get_node("Torso/RocketOrigin").global_transform)

		if Input.is_action_just_pressed("grapple"):
			grapple_ray.force_raycast_update()

			if grapple_ray.is_colliding():
				is_grappling = true

		if Input.is_action_just_pressed("jump"):
			if is_on_floor():
				vel.y = JUMP_SPEED
				is_jumping = true

		rset_unreliable("network_position", global_transform)
		rset_unreliable("network_motion", motion)
	else:
		global_transform = network_position
		motion = network_motion

func processMovement(delta):
	if is_network_master():
		if is_grappling:
			vel = grapple_ray.get_global_transform().origin.direction_to(grapple_ray.get_collision_point()) * 50			

			if get_global_transform().origin.distance_to(grapple_ray.get_collision_point()) < 20:
				is_grappling = false
		else:
			motion.y = 0
			motion = motion.normalized()

			vel.y += delta * GRAVITY

			var hvel = vel
			hvel.y = 0

			var target = motion
			var accel = 0
			if is_on_floor():
				target *= MAX_SPEED
				if motion.dot(hvel) > 0:
					accel = ACCEL
				else:
					accel=DEACCEL
			else:
				target *= MAX_AIR_SPEED
				accel = ACCEL

			hvel = hvel.linear_interpolate(target, accel * delta)
			vel.x = hvel.x
			vel.z = hvel.z
		vel = move_and_slide(vel, Vector3(0, 1, 0), true, 4, deg2rad(MAX_SLOPE_ANGLE))

func processRespawn(delta):
	respawn_timer += delta
	if respawn_timer >= RESPAWN_TIME:
		respawn_timer = 0
		rpc("respawn")

master func take_damage(damage, transform, from_who):
	if not is_dead:
		health -= damage
		if health <= 0:
			GameState.game_mode.register_kill(from_who, player_id)
			rpc("dead")
		get_tree().get_root().get_node("UI").update_health(health)

remotesync func fire(p_id, transform):
	var shot = rocket.instance()
	shot.player = p_id
	get_tree().get_root().add_child(shot)

	shot.global_transform = transform

remotesync func dead():
	get_node("BodyCollision").disabled = true
	get_node("FeetCollision").disabled = true
	hide()
	if is_network_master():
		is_dead = true
		get_tree().get_root().get_node("UI").update_health(0)
		get_tree().get_root().get_node("UI").show_dead()
		respawn_timer = 0
		GameState.game_mode.enter_spawn_queue(player_id)

remotesync func respawn():
	show()
	get_node("BodyCollision").disabled = false
	get_node("FeetCollision").disabled = false
	if is_network_master():
		is_dead = false
		health = 100
		get_tree().get_root().get_node("UI").update_health(health)
		get_tree().get_root().get_node("UI").show_hud()
		respawn_timer = 0
		set_spawn_location(GameState.game_mode.spawn_queue[player_id])
		GameState.game_mode.exit_spawn_queue(player_id)

func set_spawn_location(spawn_id):
	var spawn_loc = get_tree().get_root().get_node("Level/Spawn Points/" + str(spawn_id)).global_transform
	global_transform = spawn_loc
	rset_unreliable("network_position", global_transform)

func generate_cable():
	var verts = PoolVector3Array()
	verts.push_back(get_node("Torso/GrappleOrigin").get_global_transform().origin)
	verts.push_back(grapple_ray.get_collision_point())
	var array_mesh = ArrayMesh.new()
	var arrays = []
	arrays.resize(ArrayMesh.ARRAY_MAX)
	arrays[ArrayMesh.ARRAY_VERTEX] = verts
	array_mesh.add_surface_from_arrays(Mesh.PRIMITIVE_LINES, arrays)
	var mesh = MeshInstance.new()
	mesh.set_mesh(array_mesh)
	
	return mesh
