extends Node

var kills = {}
var deaths = {}
var scores = {}

var level_queue = []
var current_level = ""
var num_spawn_points = 0
var spawn_queue = {}

var in_progress = false

var game_mode_name = ""

func load_next_level():
	if level_queue.size() > 0 || (not current_level.empty() && in_progress):
		if not in_progress:
			current_level = level_queue.pop_front()
		var map = load("res://Levels/" + current_level + "/" + current_level + ".tscn").instance()
		get_tree().get_root().add_child(map)

		num_spawn_points = get_node("/root/Level/Spawn Points").get_child_count()
		if get_tree().is_network_server():
			kills = {}
			deaths = {}
			scores = {}
			spawn_queue = {}

		for player_id in GameState.players:
			add_player(player_id)

remotesync func add_player(player_id):
	if get_tree().is_network_server():
		kills[player_id] = 0
		deaths[player_id] = 0
		scores[player_id] = 0
		enter_spawn_queue(player_id)

	var player_node = load("res://Player.tscn").instance()
	player_node.rocket = load("res://Rocket.tscn")
	player_node.set_name(str(player_id))
	player_node.set_network_master(player_id)
	player_node.player_id = player_id
	var map = get_node("/root/Level")
	map.get_node("Players").add_child(player_node)
	if player_id == get_tree().get_network_unique_id():
		player_node.get_node("Torso/Camera").make_current()
	if not in_progress or (in_progress and player_id == get_tree().get_network_unique_id()):
		player_node.global_transform = map.get_node("Spawn Points/" + str(spawn_queue[player_id])).global_transform

remote func enter_spawn_queue(player_id:int):
	if not get_tree().is_network_server():
		rpc_id(1, "enter_spawn_queue", player_id)

	var rng = RandomNumberGenerator.new()
	rng.randomize()

	var spawn_id = rng.randi_range(0, num_spawn_points - 1)
	while spawn_queue.has(player_id) and spawn_queue[player_id] == spawn_id:
		rng.randomize()
		spawn_id = rng.randi_range(0, num_spawn_points - 1)
	spawn_queue[player_id] = spawn_id

	rpc("sync_spawn_queue", spawn_queue)

remote func exit_spawn_queue(player_id):
	if not get_tree().is_network_server():
		rpc_id(1, "exit_spawn_queue", player_id)

	spawn_queue.erase(player_id)
	rpc("sync_spawn_queue", spawn_queue)


# Sync Methods

remote func sync_level_data(server_level_list, server_current_level):
	level_queue.clear()
	level_queue = server_level_list.duplicate(true)

	current_level = server_current_level

remote func sync_spawn_queue(server_spawn_queue):
	spawn_queue.clear()
	spawn_queue = server_spawn_queue.duplicate(true)

remote func sync_in_progress(server_progress):
	in_progress = server_progress

remote func sync_score_data(server_kills, server_deaths, server_scores):
	kills.clear()
	kills = server_kills.duplicate(true)

	deaths.clear()
	deaths = server_deaths.duplicate(true)

	scores.clear()
	scores = server_scores.duplicate(true)
	GameState.emit_signal("score_updated")

func sync_all():
	rpc("sync_level_data", level_queue, current_level)
	rpc("sync_spawn_queue", spawn_queue)
	rpc("sync_in_progress", in_progress)
	rpc("sync_score_data", kills, deaths, scores)

func sync_all_id(id):
	rpc_id(id, "sync_level_data", level_queue, current_level)
	rpc_id(id, "sync_spawn_queue", spawn_queue)
	rpc_id(id, "sync_in_progress", in_progress)
	rpc_id(id, "sync_score_data", kills, deaths, scores)
