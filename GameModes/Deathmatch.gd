extends "res://GameModes/GameMode.gd"

var time_limit = 0
var score_limit = 10

func _init():
	game_mode_name = "Deathmatch"

remote func register_kill(killer_id, victim_id):
	if not get_tree().is_network_server():
		rpc_id(1, "register_kill", killer_id, victim_id)

	kills[killer_id] = kills[killer_id] + 1
	deaths[victim_id] = deaths[victim_id] + 1
	scores[killer_id] = scores[killer_id] + 1
	GameState.emit_signal("score_updated")

	if scores[killer_id] >= score_limit:
		in_progress = false
		rpc("sync_all")
		GameState.end_game()
	else:
		rpc("sync_score_data", kills, deaths, scores)
